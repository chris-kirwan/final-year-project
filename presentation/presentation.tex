\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usetheme{Copenhagen}
\usefonttheme[onlymath]{serif}


%Information to be included in the title page:
\title[$O(n)$ models]{Numerical Investigations of Phase Transitions in Spin $O(n)$ Models}
\author[Christopher Kirwan]{Christopher Kirwan \\ Project Supervisor: Sinéad Ryan}

\date{Feb 15 2021}

\begin{document}

\frame{\titlepage}

\begin{frame}
	\begin{figure}
	\centering
	\includegraphics[width=0.9\textwidth]{combined.png}
	\caption{Thermalization of the $2D$ XY Model on a $256 \times 256$ lattice. Initial random 
		configuration of spins using the Metropolis Algorithm at a temperature of $T=0.04$ for 
		approximately $15000$ Monte-Carlo steps. Note the abundance of anti-vortex/vortex pairs. 
		Colours represent $\theta \in [-\pi,\pi]$ }
	\end{figure}
\end{frame}    


\begin{frame}
\frametitle{Introduction \& Motivation}

\begin{itemize}
	\item Spin models were introduced to help describe ferromagnetic phemonea. 
	\item Representing the \textit{magnetic dipole moment} as a discrete variable $s = \pm 1$, 
		the \textit{Ising Model} was first introduced by Lenz (1920). 
	\item $1$-dimensional Free Energy solved: Ising (1925) - no phase transition. 
	\item $2$-dimensional Free Energy solution: Onsager (1944) - 1st order phase transition. 
	\item Generalizing the Ising Model, the \textit{XY Model} instead models these spins as a continuous
		variable, taking any angle along the plane. 
	\item Incredibly useful for describing systems that have a $O(2)$ or $U(1)$ symmetry, such 
		as a planar superfluid Helium. 
	\item This model helps decribe the \textit{BKT transition} - example of a topological 
		phase transition (Noble Prize 2016)
\end{itemize}
\end{frame}

\begin{frame}
	\begin{itemize}
		\item Due to the difficulty of obtaining analytic results for even the most reasonable
			lattice models, we use computational methods to obtain approximate solutions. 
		\item Computer simulations can also give plentiful results in varying parameters of those models,
			such as lattice size scaling. 
		\item Research into different algorithms also provide much research.
	\end{itemize}
\end{frame}

\begin{frame}
\frametitle{General Definitions}
	\begin{itemize}
	\item The $O(n)$ models consist of a $d$-dimensional lattice, $S$, usually $\mathbb{Z}^d$. Finite 
		subsets are typically labelled $\Lambda$. 
	\item At each vertex $i$ of the lattice, we associate a random $n$-dimensional unit vector 
		$\mathbf{s}_i$ called the spin. 
	\item Correspondinly, the entire configuration space of spins on $\Lambda$ is 
		labelled $\Omega^{\Lambda}$. 
	\item We assume \textit{nearest neighbours interaction}, and that the interaction is invariant 
		under simultaneous rotations of all the spins.  
	\item The Hamiltonian of an $O(n)$-Symmetric model (defined on  a lattice $\Lambda$) is given by
	\begin{equation}
		H = \sum_{\left \langle i,j \right \rangle} U\left( \mathbf{s}_i \cdot \mathbf{s}_j \right)
	\end{equation}
	With $U:[-1,1] \rightarrow \mathbb{R}$ usually called the \textit{potential function}.
	\end{itemize}
\end{frame}

\begin{frame}
\begin{itemize}
\item Letting $U(x) = -Jx$, we then get the Hamiltonian for the typical $O(n)$ model
	\begin{equation}
		H = -\sum_{\left \langle i,j \right \rangle} J_{ij} \ \mathbf{s}_i \cdot \mathbf{s}_j
	\end{equation}
	\item The system configurations are randomly chosen from the \textit{probability measure} (usually
			referred to as the Gibbs measure)
		$\mu_{n,\beta}$ given by
	\begin{equation}
		\mathrm{d}\mu_{n,\beta} = \frac{1}{Z_{n,\beta}} \exp \left( -\beta H\right) \prod_i \mathrm{d}s_i 
	\end{equation}
	with the \textit{partition function} defined as the normalizing constant
	\begin{equation}
		Z_{n,\beta} = \int_{\Omega_{\Lambda}} \exp \left( -\beta H\right) \prod_i \mathrm{d}s_i
	\end{equation}
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{figure}
		\centering
		\includegraphics[width=0.6\textwidth]{screenshot3.png}
		\caption{Sample configuration of an XY Model system, at a high temperature $\approx 2T$}
	\end{figure}
\end{frame}

\begin{frame}
	\begin{itemize}
		\item Often, \textit{boundary conditions} are employed on these systems. These identify 
			the spins on the boundary of the lattice with some external factor. 
			\begin{itemize}
			\item \textit{free} boundary conditions
			\item $+,-$ boundary conditions
			\item \textit{periodic} boundary conditions, which identifies the lattice as a $d$-dimensional
				torus
			\end{itemize} 
	\end{itemize}
	
	\begin{figure}[ht]
        \begin{minipage}[b]{0.45\linewidth}
            \centering
            \includegraphics[width=\textwidth]{screenshot1.png}
            \caption{$+$ boundary condition}
            \label{fig:a}
        \end{minipage}
        \hspace{0.5cm}
        \begin{minipage}[b]{0.45\linewidth}
            \centering
            \includegraphics[width=\textwidth]{screenshot2.png}
            \caption{Periodic boundary condition}
            \label{fig:b}
        \end{minipage}
    \end{figure}
\end{frame}

\begin{frame}
	\begin{itemize}
		\item The final object of interest is the 2-point correlation function
		\begin{equation}
			C(i,j) = \left \langle \mathbf{s}_i \cdot \mathbf{s}_j \right \rangle 
		\end{equation}
	Setting $n$ to various integers, we get several important cases:
		\item $n=1$ The \textit{Ising Model}, with spins taking values $\pm1$. 
		\item $n=2$ The \textit{XY Model}, with spins distributed by an angle $\theta \in [-\pi,\pi]$
		\item $n=3$ The \textit{Hesienberg Model}, with the distribution of spins being $S^2$. 
	\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Metropolis Algorithm} 
\begin{itemize}
	\item The \textit{Metropolis Algorithm} is a method to obtain a \textit{Markov Chain} (sequence of 
		random states) from a given probability distribution. 
	\item Assuming a selection probability $g(\mu,\nu) = \frac{1}{L}$ (going from a state $\mu \rightarrow
		\nu$), and satisfying \textit{detailed balance}, the acceptance algorithm is
		
		\begin{equation}
		A\left ( \mu,\nu \right ) = \left\{\begin{matrix}
 			e^{-\beta \left ( H_{\nu} - H_{\mu} \right )}& \mathrm{if} H_{\nu} - H_{\mu} > 0\\ 
 			1 & \mathrm{otherwise}
		\end{matrix}\right.
		\end{equation}
	\item Using this algorithm, estimates of observables $\mathcal{O}$ on the lattice can be made
\end{itemize}
\end{frame}

\begin{frame}
	\begin{itemize}
		\item Entries in the chain (observables) are not statistically dependent $\rightarrow$ care 
			must taken when sampling the chain and computing errors. 
		\item Given some series of observables labelled in a temporal order $i=1,2,...,N$, we define 
			the \textit{autocorrelation function} of the observable as 
			\begin{equation}
				A(t) = A_{ij} = 
				\left \langle \left(\mathcal{O}_i - \left \langle \mathcal{O}_i \right \rangle\right) 
			\left (\mathcal{O}_j - \left \langle \mathcal{O}_j \right \rangle\right)\right \rangle 
		\end{equation}
		\[= \left \langle \mathcal{O}_0 \mathcal{O}_t \right \rangle - 
		\left \langle \mathcal{O}_0 \right \rangle\left \langle \mathcal{O}_t \right \rangle \]

		\item For large $t$, the behaviour of the autocorrelation function follows
			\begin{equation}
				A(t) \sim   \exp\left( \frac{-t}{\tau} \right)
			\end{equation}
			and we denote $\tau$ as the \textit{autocorrelation time}. 
		\item Finally, errors are calculated using the \textit{Jackknife Resampling} method
		
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{autocorrelation.png}
	\caption{Magnetization Autocorrelation of the Ising Model. Notice how the Correlation time increases
	as we approach $T_c$}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{The Ising Model}
	\begin{itemize}
		\item The Hamiltonian of the Ising Model, including a term for an interacting magnetic field is
			\begin{equation}
			H = -\sum_{\left \langle i,j \right \rangle} J_{ij}s_i s_j - \mu\sum_{j} h_j s_j; 
			\end{equation}
		(In the Monte-Carlo simulations, the magnetic field term was ignored, and $J_{ij}$ was simplifed
		to $1$ for the entire lattice. Periodic Boundary Conditions were also employed)

		\item In the thermodynamic limit, with $h=0$, we can obtain an analytic expression for the free
			energy:
			\begin{equation}
			\begin{aligned}
				-\beta f = \ln(2) + & \frac{1}{8\pi^2}\int_0^{2\pi} d\theta_1 \int_0^{2\pi} d\theta_2 
				\ln(\cosh^2(2\beta) \\
				& - \sinh( \cos(\theta_1) + cos(\theta_2)))
			\end{aligned}
			\end{equation}
		\item Since this fails to differentiable at $\beta = \beta_c$, we then have a first-order 
			transition. 
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{itemize}
		\item The difficulty of these calculations $\rightarrow$ more systematic method. 
		\item If at least two distinct Gibbs measures can be constructed for a pair
			$(T,h)$, we say that there is a first-order phase transition at $(T,h)$. 
	\end{itemize}
	\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{phasediagram.png}
	\caption{Phase Diagram of the Ising Model in $d \geq 2$.}
	\end{figure}
\end{frame}

\begin{frame}
	\begin{itemize}
	\item Defining a quantity called \textit{magnetization}, with
		\[ \langle M \rangle = \frac{1}{N}\sum_i s_i \]
			we see in the high-temperature limit, the non-uniquness of the Gibbs measures 
			$\mu_{\beta}^{+}$ and $\mu_{\beta}^{-}$ imply that $\left\langle M \right\rangle = 0$. 
		\item But, in the low-temperature linit, $\left\langle M \right\rangle \neq 0$ and we have 
			a broken symmetry at finite $T$.  	
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Monte-Carlo Results}
		\begin{itemize}
			\item Defining the energy as $\langle E \rangle = \frac{1}{2N}\langle \sum_{\langle i,j 
				\rangle} s_i s_j \rangle$, the simulations are below. 
		\end{itemize}

	\begin{figure}[ht]
       \begin{minipage}[b]{0.45\linewidth}
            \centering
            \includegraphics[width=\textwidth]{isingmagnetization.png}
            \caption{Magnetization as function of Temperature}
        \end{minipage}
        \hspace{0.5cm}
        \begin{minipage}[b]{0.45\linewidth}
            \centering
            \includegraphics[width=\textwidth]{isingenergy.png}
            \caption{Energy as a function of Temperature}
        \end{minipage}
    \end{figure}
\end{frame}


\begin{frame}
	\begin{itemize}
		\item Heat Capacity $C = \frac{\langle E^2 \rangle - \langle E \rangle^2}{k_b T^2}$ 
		\item Binder Cumulant $ U_4 = 1 - \frac{\langle M^4 \rangle_L}{3 \langle M^2\rangle^2_L }$
	\end{itemize}

	\begin{figure}[ht]
       \begin{minipage}[b]{0.45\linewidth}
            \centering
            \includegraphics[width=\textwidth]{isingspecific-heat.png}
            \caption{Specific Heat as function of Temperature}
        \end{minipage}
        \hspace{0.5cm}
        \begin{minipage}[b]{0.45\linewidth}
            \centering
            \includegraphics[width=\textwidth]{isingbinder.png}
            \caption{Binder Cumulant as function of Temperature}
        \end{minipage}
    \end{figure}
\end{frame}

\begin{frame}
	\frametitle{The XY Model}

	\begin{itemize}
		\item The Hamiltonian of the XY model is
			\begin{equation}
			\begin{aligned}
				H &= -\sum_{\left \langle i,j \right \rangle} J_{ij} \mathbf{s}_i \cdot \mathbf{s}_j - 
				\sum_j \mathbf{h}_j \cdot \mathbf{s}_j \\
				  &= -\sum_{\left \langle i,j \right \rangle} J_{ij} 
				\cos(\theta_i - \theta_j) - \sum_j h_j \cos(\theta_j)
			\end{aligned}
			\end{equation}
			with the same simplifications taken for the computational aspects. 
		\item The partition function follows straight-forwardly 
			\begin{equation}
				Z = \prod_i \int_0^{2\pi} \frac{d\theta_i}{2\pi} \exp({-\beta H})
			\end{equation}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Monte-Carlo Results}

	\begin{itemize}
		\item We define the magnetization squared as follows
			\begin{equation}
				\langle M^2 \rangle = \left\langle \left(\sum_i^N \cos(\theta_i) \right) ^2 
				+ \left(\sum_i^N \sin(\theta_i) \right) ^2\right\rangle
			\end{equation}
			With the definition of the energy remaining the same.
	\begin{figure}[ht]
       \begin{minipage}[b]{0.39\linewidth}
            \centering
            \includegraphics[width=\textwidth]{xymagnetization.png}
            \caption{Magnetization as function of Temperature}
        \end{minipage}
        \hspace{0.5cm}
        \begin{minipage}[b]{0.39\linewidth}
            \centering
            \includegraphics[width=\textwidth]{xyenergy.png}
            \caption{Energy as a function of Temperature}
        \end{minipage}
    \end{figure}

	\end{itemize}


\end{frame}

%\begin{frame}
%	\frametitle{Mermin-Wagner Theorem}
%
%\item This theorem states a continuous symmetry cannot be broken at any finite temperature cannot
%	be broken spontaneously in $d \leq 2$. 
%\end{frame}

\begin{frame}
	\frametitle{The BKT Transition}
	\begin{itemize}

	\item A 2nd-Order phase transition cannot occur in the XY Model, due to the expected ordered phase 
		being destroyed by traverse fluctuations (the Goldstone bosons associated with the broken $U(1)$
		symmetry - Mermin-Wagner Theorem)

	\item High-Temperature Expansion of Partition Function and Correlation Function
		\begin{equation}
			 Z = \prod_{\langle i,j \rangle} \int_0^{2\pi} \frac{d\theta_i}{2\pi} (1 + K\cos(\theta_i - \theta_j) + \mathcal{O}(K^2))
		\end{equation}

		From this, the correlation function can be estimated as
		\begin{equation}
			\langle \mathbf{s}_0 \cdot \mathbf{s}_r\rangle = \langle \cos(\theta_r - \theta_0) \rangle
			\propto \left( \frac{K}{2} \right)^{|r|} = \exp\left( \frac{-|r|}{\xi}\right)
		\end{equation}
		where $\xi = \ln\left( \frac{2}{K}\right)$, and thus an exponential decay occurs at high T.
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{itemize}
		\item Low-Temperature Expansion: Assume a small angle fluctuation and take the continuum limit
		\begin{equation}
			\theta_i - \theta_j = a \nabla \theta \cdot \hat{e}_{ij} + \mathcal{O}(a^2)
		\end{equation}
		To get the Hamiltonian 
		\begin{equation}
			\beta H = \frac{\beta}{2} \int dx^2 (\nabla \theta)^2
		\end{equation}
		\item Quadratic Hamiltonian $\Rightarrow$ Gaussian approximation for the Correlation function.
		\begin{equation}
		\begin{aligned}
			\langle \mathbf{s}_0 \cdot \mathbf{s}_r \rangle &= 
			\mathfrak{Re} \langle \exp(i(\theta(0) - \theta(r)) \rangle \\
			&= \mathfrak{Re} \left( \exp \left( -\frac{\langle (\theta(0) - \theta(r))^2\rangle}{2} \right)  \right)
		\end{aligned}
		\end{equation}
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{itemize}
		\item The correlation function of the low temperature case decays algebraically as
			\begin{equation}
				\langle \mathbf{s}_0 \cdot \mathbf{s}_r\rangle \approx \left( \frac{\alpha}{|r|}\right)^{
			\frac{1}{2\pi K}}
			\end{equation}

		\item This then suggests that we've got a quasi-long ranged order. This is indicative of 
			a phase thransition. 
		\item However, due to the Mermin-Wagner theorem, we cannot have a symmetry breaking.
		\item Kosterlitz and Thouless (1973)  proposed a solution: topological defects 
			explain the phase transition
	\end{itemize}
\end{frame}


\begin{frame}
	\begin{itemize}
		\item These defects are vorties and anti-vortices
		\item These configurations never appear in the perturbative gradient expansion and are hence 
			non-perturbative in nature. 
		\item Nonetheless, we can use the Euler-Lagrange equation of a single vortex to calculate it's
			energy contribution $E = \kappa \ln\left( \frac{L}{a} \right)$
		\item At finite $T$, considering entropic contributions, we get that the Free Energy becomes
			\begin{equation}
				F = E - TS = (\kappa - 2k_B T)\ln\left(\frac{L}{a} \right)
			\end{equation}
		\item $F < 0 \Rightarrow $ proliferation of vortex/antivortex unbindings!
	\end{itemize}

	\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{vortex.jpg}
	\end{figure}

\end{frame}	


\begin{frame}
	\frametitle{Future Work}

	\begin{itemize}
		\item Finite size scaling
		\item Implement other algorithms, such as Wolff cluster to reduce autocorrelations in the systems.
		\item Implement studies on other graphs (bipartite vs. non-bipartite). The Ising Model on the 
			Bethe Lattice has an analytic solution to the free energy. 
	\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Bibliography}

	\begin{itemize}
		\item Newman M. E. J., Barkema G. T., (1999) \textit{Monte Carlo Methods in Statistical Physics}, Clarendon Press.
		\item Baxter, Rodney J. (1982), \textit{Exactly solved models in statistical mechanics}, London: Academic Press Inc.
		\item Peierls, R.; Born, M. (1936). \textit{On Ising's model of ferromagnetism}. Mathematical Proceedings. \textbf{32} (3): 477. 
		\item Friedli, S. and Velenik, Y. (2017) \textit{Statistical Mechanics of Lattice Systems}, Cambridge University Press
		\item Ota, S.B.; Fahnle, M (1992). \textit{Microcanonical Monte Carlo simulations for the two-dimensional XY model}. J. Phys.: Condens. Matter. 4: 5411
		\item Fröhlich, J.; Spencer, T. (1981). \textit{The Kosterlitz–Thouless transition in two-dimensional abelian spin systems and the Coulomb gas}. Comm. Math. Phys. 81 (4): 527–602
		\item  Kosterlitz, J. M.; Thouless, D. J. (November 1972). \textit{Ordering, metastability and phase transitions in two-dimensional systems}. Journal of Physics C: Solid State Physics. 6 (7): 1181–1203
	\end{itemize}
\end{frame}

\end{document}
