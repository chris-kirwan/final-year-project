#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include "../wolff.h"

double ArrayAverage(int l, double arr[]) {
	int i;
	double sum = 0.0; 	
	for(i=0;i<l;i++) {
		sum += arr[i]; 
	}
return sum/l; 
}

void Jackknife(int len, double a_1[], double a_2[], int jack) {
	int i;

	for(i=0;i<jack-1;i++) {
		a_2[i] = a_1[i];
	}
	for(i=jack-1;i<len-1;i++) {
		a_2[i] = a_1[i+1];
	}
}

double JackErr(int len, double a[]) {
	double mean = ArrayAverage(len,a);
	double sumsq = 0.0;

	double *jack_arr = malloc((len-1) * sizeof(double));
	for(int i=0;i<len;i++) {
		Jackknife(len,a,jack_arr,i+1);
		double jack_mean = ArrayAverage(len-1,jack_arr);
		sumsq += (jack_mean - mean) * (jack_mean -mean);
	}

	sumsq *= (double)(len-1)/((double)len);
free(jack_arr); 
return sqrt(sumsq); 
}

void BinArray(int len, double a[], double b[], int bin_size) {
	int i,j,k=0;

	for(i=0;i<len;i+=bin_size) {
		double sum = 0.0;
		int count = 0;

		for(j=0;j<bin_size;j++) {
			sum += a[i+count];
			count++;
		}
		b[k] = sum/bin_size;
		k++; 
	}
}

int main(int argc, char* argv[]) {
	int l = atoi(argv[1]);  
	int n = l*l; 
	int i,j; 
 	
	int thermal = atoi(argv[2]); 
	int wolff_steps = atoi(argv[3]);	
	int bin_size = 100; 

	int (*F)[l] = malloc(sizeof(int[l][l])); 
	bool (*C)[l] = malloc(sizeof(bool[l][l])); 

	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937); 
	unsigned long int seed = (unsigned)time(NULL); 
	gsl_rng_set(r, seed); 

	double T; 
	for(T=2.0;T<2.8;T+=0.005) {
		double *mag_arr = malloc(wolff_steps * sizeof(double)); 
		double *mag_sqd = malloc(wolff_steps * sizeof(double)); 
		double *bin_arr_1 = (double*) malloc(wolff_steps/bin_size * sizeof(double));
		double *bin_arr_2 = (double*) malloc(wolff_steps/bin_size * sizeof(double));
		ColdLattice(l,F); 

		for(i=0;i<thermal;i++) {
			StartCluster(l,F,C,T,r);
		}

		for(i=0;i<wolff_steps;i++) {
			StartCluster(l,F,C,T,r); 
			double ans = MagnetizationPerSpin(l,F); 
			mag_arr[i] = fabs(ans);
			mag_sqd[i] = ans*ans; 
		}
		BinArray(wolff_steps,mag_arr,bin_arr_1,bin_size);
		BinArray(wolff_steps,mag_sqd,bin_arr_2,bin_size);
		double err_1 = JackErr(wolff_steps/bin_size, bin_arr_1);
		double err_2 = JackErr(wolff_steps/bin_size, bin_arr_2);
		double mag2 = ArrayAverage(wolff_steps,mag_sqd); 
		double mag = ArrayAverage(wolff_steps,mag_arr); 

		double ans = (n/T) * (mag2 - mag*mag);
		double err = (n/T) * sqrt(4*mag*mag*err_1*err_1 + err_2*err_2);
		printf("%lf %lf %lf\n", T,ans,err);

	free(mag_arr);	
	free(mag_sqd); 
	free(bin_arr_1); 
	free(bin_arr_2); 
	}

free(C); 
free(F); 

return 0; 
}


