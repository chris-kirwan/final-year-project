#! /bin/bash

./susceptibility 4 200 2000 > susceptibility_L4.dat
./susceptibility 8 400 3000 > susceptibility_L8.dat
./susceptibility 16 500 10000 > susceptibility_L16.dat
./susceptibility 24 1000 20000 > susceptibility_L24.dat
./susceptibility 32 2000 30000 > susceptibility_L32.dat
./susceptibility 64 3000 40000 > susceptibility_L64.dat
