#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <gsl/gsl_rng.h>

void GrowCluster(int l, int i, int j, int value, int F[][l], bool C[][l], double T, gsl_rng *r);
void AttemptToAdd(int l, int i, int j, int value, int F[][l], bool C[][l], double T,gsl_rng *r);

void ColdLattice(int l, int F[][l]) {
	int i,j;

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			F[i][j] = 1;
		}
	}
}

void RandomLattice(int l, int F[][l], gsl_rng*r) {
	int i,j;

	 for(i=0;i<l;i++) {
		 for(j=0;j<l;j++) {
			  F[i][j] = 2*(int)gsl_rng_uniform_int(r,2)-1;
		}
	}
}

void StartCluster(int l, int F[][l], bool C[][l], double T,gsl_rng*r) {
	int i,j; 
	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			C[i][j] = false; 
		}
	}

	int x = gsl_rng_uniform_int(r,l); 
	int y = gsl_rng_uniform_int(r,l); 
	int val = F[x][y]; 
	GrowCluster(l,x,y,val,F,C,T,r); 

	int size = 0; 
	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			size++; 
		}
	}
}

void GrowCluster(int l, int i, int j, int value, int F[][l], bool C[][l], double T, gsl_rng *r) {
	C[i][j] = true;
   	F[i][j] *= -1; 	

	int left = i?(i-1):(l-1); 
	int right = (i+1)%l; 
	int up = j?(j-1):(l-1); 
	int down = (j+1)%l; 
	
	if(!C[left][j]) {AttemptToAdd(l,left,j,value,F,C,T,r);}
	if(!C[right][j]) {AttemptToAdd(l,right,j,value,F,C,T,r);}
	if(!C[i][up]) {AttemptToAdd(l,i,up,value,F,C,T,r);}
	if(!C[i][down]) {AttemptToAdd(l,i,down,value,F,C,T,r);}
}

void AttemptToAdd(int l, int i, int j, int value, int F[][l], bool C[][l], double T,gsl_rng *r) {
	double probability = 1-exp(-2.0/T); 
	double random = gsl_rng_uniform(r); 

	if(F[i][j] == value) {
		if(random < probability) {
			GrowCluster(l,i,j,value,F,C,T,r); 
		}
	}
}

double MagnetizationPerSpin(int l, int F[][l]) {
	int i,j; 
	int n = l*l; 

	double magnetization = 0.0; 
	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			magnetization += F[i][j]; 
		}
	}
return magnetization/n; 
}

double EnergyAtSite(int l, int i, int j, int F[][l]) {
	int energy = -F[i][j]*(F[i?(i-1):(l-1)][j] + F[(i+1)%l][j]
					+ F[i][j?(j-1):(l-1)] +  F[i][(j+1)%l]); 
return energy; 
}

double TotalEnergyPerSpin(int l, int F[][l]) {
	int n = l*l; 
	int i,j; 
	int total_energy = 0; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			total_energy += 0.5*EnergyAtSite(l,i,j,F);
		}
	}
return (double)total_energy/n; 
}

