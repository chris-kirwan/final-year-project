#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include "../wolff.h"

int main(int argc, char* argv[]) {
	int l = 25; 
	int n = l*l; 
	int i,j; 

	double T = atof(argv[1]); 
	int wolff_steps = 2500; 

	int (*F)[l] = malloc(sizeof(int[l][l])); 
	bool (*C)[l] = malloc(sizeof(bool[l][l])); 

	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937); 
	unsigned long int seed = (unsigned)time(NULL); 
	gsl_rng_set(r, seed); 

	ColdLattice(l,F);
	double *mag_arr = malloc(wolff_steps * sizeof(double)); 

	for(i=0;i<wolff_steps;i++) {
		StartCluster(l,F,C,T,r); 
		mag_arr[i] = fabs(MagnetizationPerSpin(l,F)); 
	}

	for(i=0;i<wolff_steps;i++) {
		printf("%d %lf\n", i, mag_arr[i]); 
	}
	printf("\n"); 

free(mag_arr); 
free(C); 
free(F); 

return 0; 
}


