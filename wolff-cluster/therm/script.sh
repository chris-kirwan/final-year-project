#! /bin/bash

./thermalization 0.5 > thermalization_050.dat
./thermalization 1.0 > thermalization_100.dat
./thermalization 2.1 > thermalization_210.dat
./thermalization 3.0 > thermalization_300.dat
./thermalization 5.0 > thermalization_500.dat

gnuplot thermalization.p
