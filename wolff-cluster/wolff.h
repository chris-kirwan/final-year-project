#ifndef WOLFF_H

#define WOLFF_H

void ColdLattice(int l, int F[][l]); 
void RandomLattice(int l, int F[][l], gsl_rng *r); 
void StartCluster(int l, int F[][l], bool C[][l], double T, gsl_rng*r); 
double MagnetizationPerSpin(int l, int F[][l]); 
double EnergyAtSite(int l, int i, int j, int F[][l]); 
double TotalEnergyPerSpin(int l, int F[][l]); 

#endif
