#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include "../wolffcluster.h"

double Average(int len, double array[]) {
	int i;
	double sum = 0.0; 

	for(i=0;i<len;i++) {
		sum += array[i]; 
	}
return sum/len; 
}

int main(int argc, char* argv[]) {
	int l = atof(argv[1]); 
	int n = l*l; 
	int i,j; 

	int thermalization = 100;
	int monte_carlo_steps = 50000; 
	int sampling_frequency = 50; 
	int number_samples = monte_carlo_steps/sampling_frequency; 

	int (*F)[l] = malloc(sizeof(int[l][l]));
   	bool (*C)[l] = malloc(sizeof(bool[l][l])); 	

	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937); 
	unsigned long int seed = (unsigned)time(NULL); 
	gsl_rng_set(r, seed); 

	double T; 
	for(T=2.0;T<2.6;T+=0.0125) {
		ColdLattice(l,F);

	   	double mag_sqrd_array[number_samples];
	   	double mag_4th_array[number_samples]; 	
		int count = 0; 

		for(i=0;i<thermalization;i++) {
				StartCluster(l,F,C,T,r); 
		}

		for(i=0;i<monte_carlo_steps;i++) {
			StartCluster(l,F,C,T,r); 

			if(!(i%sampling_frequency)) { 
				mag_sqrd_array[count] = MagnetizationPerSpin(l,F)*MagnetizationPerSpin(l,F); 
				mag_4th_array[count] = MagnetizationPerSpin(l,F)*MagnetizationPerSpin(l,F)
										*MagnetizationPerSpin(l,F)*MagnetizationPerSpin(l,F); 
				count++; 
			}
		}
		double avg1 = Average(count, mag_sqrd_array); 
		double avg2 = Average(count, mag_4th_array);  

		printf("%lf %lf\n", T, 1 - avg2/(3.0*avg1*avg1)); 
	}

free(F); 
free(C); 
return 0; 
}

