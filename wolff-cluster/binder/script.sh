#! /bin/bash

x=(16 32 64 128)

for i in ${!x[@]}; do
	./binder ${x[$i]} > binder${x[$i]}.dat
done
