#! /bin/bash

./heatcap 4 200 20000 > heatcap_L4.dat
./heatcap 8 1000 30000 > heatcap_L8.dat
./heatcap 16 3000 40000 > heatcap_L16.dat
./heatcap 24 4000 50000 > heatcap_L24.dat
./heatcap 32 5000 60000 > heatcap_L32.dat
./heatcap 64 6000 70000  > heatcap_L64.dat
#./heatcap 128 7000 80000 > heatcap_128.dat
#./heatcap 256 8000 100000 > heatcap_256.dat

gnuplot heatcap.p
