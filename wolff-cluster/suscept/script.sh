#! /bin/bash

./susceptibility 4 200 2000 > susceptibility_L4.dat
./susceptibility 8 1000 3000 > susceptibility_L8.dat
./susceptibility 16 2000 10000 > susceptibility_L16.dat
./susceptibility 24 3000 20000 > susceptibility_L24.dat
./susceptibility 32 4000 30000 > susceptibility_L32.dat
./susceptibility 64 5000 40000 > susceptibility_L64.dat
./susceptibility 128 6000 80000 > susceptibility_L128.dat
./susceptibility 256 7000 100000 > susceptibility_L256.dat
