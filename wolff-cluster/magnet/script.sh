#! /bin/bash

./magnetization 4 200 2000 > magnetization_L4.dat
./magnetization 8 400 3000 > magnetization_L8.dat
./magnetization 16 800 8000 > magnetization_L16.dat
./magnetization 24 1600 16000 > magnetization_L24.dat
./magnetization 32 3200 32000 > magnetization_L32.dat
./magnetization 64 6400 64000 > magnetization_L64.dat
./magnetization 128 12800 128000 > magnetization_L128.dat
