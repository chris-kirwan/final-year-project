#! /bin/bash

./energy 4 200 2000 > energy_L4.dat
./energy 8 400 4000 > energy_L8.dat
./energy 16 800 8000 > energy_L16.dat
./energy 24 1200 1200 > energy_L24.dat
./energy 32 1600 16000 > energy_L32.dat
./energy 64 3200 32000 > energy_L64.dat

gnuplot energy.p
