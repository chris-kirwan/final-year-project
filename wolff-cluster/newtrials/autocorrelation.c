#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include "../wolff.h"

double Mean(double array[], int len) {
	int i; 
	double sum = 0.0; 

	for(i=0;i<len;i++) {
		sum += array[i]; 
	}
return sum/len; 
}

double AutoCorr(double array[], double len, int t) {
	int i; 
	int max_len = len-t; 

	double mu = Mean(array, len); 
	double sum = 0.0; 
	int counter = 0; 

	for(i=0;i<max_len;i++) {
		double term_1 = array[i] - mu; 
		double term_2 = array[i+t] - mu; 
		sum += term_1*term_2; 
		counter++; 
	}
return (1.0/counter)*(sum); 
}

int main(int argc, char* argv[]) {
	int l = atoi(argv[1]); 
	int n = l*l; 

	double T = atof(argv[2]); 

	int thermal = 5000; 
	int wolff_steps = atoi(argv[3]);
   	int t_max  = atoi(argv[4]); 	

	int (*F)[l] = malloc(sizeof(int[l][l])); 
	bool (*C)[l] = malloc(sizeof(bool[l][l])); 

	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937); 
	unsigned long int seed = (unsigned)time(NULL); 
	gsl_rng_set(r, seed); 

	ColdLattice(l,F);

	double *mag_arr = (double*) malloc(wolff_steps * sizeof(double)); 
	double *auto_corr = (double*) malloc(t_max * sizeof(double));

	for(int i=0;i<thermal;i++) {
		StartCluster(l,F,C,T,r); 
	}

	for(int i=0;i<wolff_steps;i++) {
		StartCluster(l,F,C,T,r); 
		mag_arr[i] = fabs(MagnetizationPerSpin(l,F)); 
	}
	
	for(int t=0;t<t_max;t++) {
		double ans = AutoCorr(mag_arr,wolff_steps, t); 
		double norm = AutoCorr(mag_arr,wolff_steps, 0); 
		auto_corr[t] = ans/norm;
	}

	for(int t=0;t<t_max;t++) {
		printf("%d %lf\n", t, auto_corr[t]); 
	}

free(mag_arr); 
free(auto_corr); 
free(F); 
free(C); 
return 0; 
}


