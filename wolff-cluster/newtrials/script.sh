#! /bin/bash

./autocorrelation 4 2.27 5000000 20 > auto_L4.dat
./autocorrelation 8 2.27 5000000 30 > auto_L8.dat
./autocorrelation 16 2.27 5000000 40 > auto_L16.dat
./autocorrelation 24 2.27 5000000 40 > auto_L24.dat
./autocorrelation 32 2.27 5000000 40 > auto_L32.dat
./autocorrelation 64 2.27 5000000 60 > auto_L64.dat
./autocorrelation 128 2.27 5000000 70 > auto_L128.dat

gnuplot autocorrelation.p
