#! /bin/bash

./energy 4 20000 > energy_L4.dat
./energy 8 30000 > energy_L8.dat
./energy 16 40000 > energy_L16.dat
./energy 24 50000 > energy_L24.dat
./energy 32 60000 > energy_L32.dat

gnuplot energy.p
