#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_rng.h>

void RandomLattice(int l, double F[][l], gsl_rng*r) {
	int i,j; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			F[i][j] = 2*M_PI*gsl_rng_uniform(r) - M_PI; 
		}
	}
}

void ColdLattice(int l, double F[][l]) {
	int i,j; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			F[i][j] = 0; 
		}
	}
}

double RandomSpin(gsl_rng *r) { 
	double new_angle = 2*M_PI*gsl_rng_uniform(r) - M_PI; 
	return new_angle; 

}

double MagnetizationSquaredPerSpin(int l, double F[][l]) {
	int i,j;
	double total; 
	double sum1 = 0.0;
	double sum2 = 0.0; 
	int n = l*l; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			sum1 += cos(F[i][j]); 
			sum2 += sin(F[i][j]); 
		}
	}
	total = (sum1*sum1 + sum2*sum2); 
return total/(n*n); 
}


double EnergyAtSite(int l, double F[][l], int i, int j) {
	double site = F[i][j];

	double left = F[i?(i-1):(l-1)][j]; 
	double right = F[(i+1)%l][j]; 
	double up = F[i][j?(j-1):(l-1)]; 
	double down = F[i][(j+1)%l]; 

	double energy = cos(site - left) + cos(site - right) + cos(site - up) + cos(site - down); 	
	return energy; 
}

double EnergyFlip(int l, double F[][l], int i, int j, double new_spin) {
	double new = new_spin;

	double left = F[i?(i-1):(l-1)][j];
	double right = F[(i+1)%l][j];
	double up = F[i][j?(j-1):(l-1)];
	double down = F[i][(j+1)%l];

	double energy_new = cos(new - left) + cos(new - right) + cos(new - up) + cos(new - down);
	return energy_new; 
}

double EnergyPerSpin(int l, double F[][l]) {
	int i,j;
	int n = l*l; 

	double enrgy = 0.0; 
	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			enrgy -=0.5*EnergyAtSite(l,F,i,j); 
		}
	}
return enrgy /= n; 
}

void MetropolisStep(int l, double F[][l], double T, gsl_rng *r) {
		int i = (int)gsl_rng_uniform_int(r,l); 
		int j = (int)gsl_rng_uniform_int(r,l); 

		double new_spin = RandomSpin(r);  
		double delta_energy = EnergyAtSite(l,F,i,j) - EnergyFlip(l,F,i,j,new_spin); 
		double boltzmann = exp(-delta_energy*(1.0)/T); 
		double random = gsl_rng_uniform(r); 

		if(delta_energy < 0) {
			F[i][j] = new_spin; 
		} else if (delta_energy >= 0) {
			if(random <= boltzmann) {
				F[i][j] = new_spin;
			} else if (random > boltzmann) {
				F[i][j] = F[i][j]; 
			}
		}
}
