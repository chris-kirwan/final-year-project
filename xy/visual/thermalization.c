#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include "../xy.h"

int main(int argc, char* argv[]) {
	int l = 25; 
	int n = l*l; 
	int i,j; 

	double T = atof(argv[1]); 

	int monte_carlo_steps = 5000; 

	double (*F)[l] = malloc(sizeof(double[l][l])); 

	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937); 
	unsigned long int seed = (unsigned)time(NULL); 
	gsl_rng_set(r, seed); 

	RandomLattice(l,F,r);

	for(i=0;i<monte_carlo_steps;i++) {
		for(j=0;j<n;j++) {
			MetropolisStep(l,F,T,r); 
		}
		double magnetization = MagnetizationSquaredPerSpin(l,F); 
		printf("%d %lf\n", i, magnetization); 
	}

return 0; 
}


