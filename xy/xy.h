#ifndef XY_H

#define XY_H

void RandomLattice(int l, double F[][l], gsl_rng*r);
void ColdLattice(int l, double F[][l]);
double RandomSpin(gsl_rng *r);
double MagnetizationSquaredPerSpin(int l, double F[][l]); 
double EnergyAtSite(int l, double F[][l], int i, int j);
double EnergyFlip(int l, double F[][l], int i, int j, double NewSpin);
double EnergyPerSpin(int l, double F[][l]); 
void MetropolisStep(int l, double F[][l], double T, gsl_rng*r);

#endif
