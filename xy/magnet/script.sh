#! /bin/bash

./magnetization 4 20000 > magnetization_L4.dat
./magnetization 8 30000 > magnetization_L6.dat
./magnetization 16 40000 > magnetization_L16.dat
./magnetization 24 50000 > magnetization_L24.dat
./magnetization 32 60000 > magnetization_L32.dat

gnuplot magnetization.p
