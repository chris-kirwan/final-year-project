#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include "../xy.h"

double ArrayAverage(int len, double array[]) {
	double sum = 0.0; 
	for(int i=0;i<len;i++) {
		sum += array[i]; 
	}
return sum/len; 
}

void Metropolis(int l, double F[][l], int t_1, int t_2, double T, double mag_arr[], gsl_rng*r) {
	int i,j; 
	int n = l*l; 
	
	ColdLattice(l,F); 

	for(i=0;i<t_1;i++) {
		for(j=0;j<n;j++) {
			MetropolisStep(l,F,T,r); 
		}
	}

	for(i=0;i<t_2;i++) {
		for(j=0;j<n;j++) {
			MetropolisStep(l,F,T,r); 
		}
		mag_arr[i] = MagnetizationSquaredPerSpin(l,F); 
	} 
}

void Jackknife(int len, double a_1[], double a_2[], int jack) {
	int i; 

	for(i=0;i<jack-1;i++) {
		a_2[i] = a_1[i]; 
	}
	for(i=jack-1;i<len-1;i++) {
		a_2[i] = a_1[i+1]; 
	}
}

double JackErr(int len, double a[]) {
	double mean = ArrayAverage(len,a); 
	double sumsq = 0.0; 

	double *jack_arr = malloc((len-1) * sizeof(double));
	for(int i=0;i<len;i++) {
		Jackknife(len,a,jack_arr,i+1);
		double jack_mean = ArrayAverage(len-1,jack_arr);
		sumsq += (jack_mean - mean) * (jack_mean -mean);
	}
	sumsq *= (double)(len-1)/((double)len);
free(jack_arr); 
return sqrt(sumsq); 
}

int main(int argc, char* argv[]) {
	int l = atoi(argv[1]); 
	int n = l*l; 
	int i,j; 

	double (*F)[l] = malloc(sizeof(double[l][l])); 
	
	int therm = 5000; 
	int mc_steps = atoi(argv[2]);
	int bin_size = 1000; 

	 gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);
	 unsigned long int seed = (unsigned)time(NULL);
	 gsl_rng_set(r, seed);
	
	double T; 

	for(T=0.1;T<2.0;T+=0.01) {
		double *mag_arr = (double*) malloc(mc_steps * sizeof(double));
		double *bin_arr = (double*) malloc(mc_steps/bin_size * sizeof(double)); 
		Metropolis(l,F,therm,mc_steps,T,mag_arr,r); 
		
		int k = 0; 
		for(int i=0;i<mc_steps;i+=bin_size) {
			double sum = 0.0; 
			int count = 0; 
			for(int j=0;j<bin_size;j++) {
				sum += mag_arr[i+count]; 
				count++; 
			}
			bin_arr[k] = sum/bin_size; 
			k++; 
		}

		double mag = ArrayAverage(mc_steps,mag_arr); 
		double err = JackErr(mc_steps/bin_size, bin_arr); 
		printf("%lf %lf %lf\n", T,mag,err); 
		
		free(mag_arr); 
	}

free(F); 
return 0; 
}
