#! /bin/bash

./thermalization 0.5 > thermalization_050.dat
./thermalization 1.0 > thermalization_100.dat
./thermalization 1.5 > thermalization_150.dat

gnuplot thermalization.p
