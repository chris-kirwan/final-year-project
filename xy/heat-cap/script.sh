#! /bin/bash

./heatcap 4 1000 50000 > heatcap_L4.dat
./heatcap 16 2000 70000 > heatcap_L16.dat
./heatcap 24 3500 100000 > heatcap_L24.dat
./heatcap 32 5000 250000 > heatcap_L32.dat

gnuplot heatcap.p
