#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_rng.h>

void ColdLattice(int l, int F[][l]) {
	int i,j; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			F[i][j] = 1; 
		}
	}
}

void RandomLattice(int l, int F[][l], gsl_rng*r) {
	int i,j; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			F[i][j] = 2*(int)gsl_rng_uniform_int(r,2)-1; 
		}
	}
}

double MagnetizationPerSpin(int l, int F[][l]) {
	int i,j;
	int n = l*l; 

	double magnetization = 0.0; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			magnetization += F[i][j];
		}
	}	
return  magnetization/n;  
}

double EnergyAtSite(int l, int i, int j, int F[][l]) {
	int k; 
	int nns = 4; 
	int site = F[i][j]; 
	int nn_arr[] = {F[i?(i-1):(l-1)][j], F[(i+1)%l][j], F[i][j?(j-1):(l-1)], F[i][(j+1)%l]}; 

	int energy = 0; 
	for(k=0;k<nns;k++) {
		energy -= site*nn_arr[k]; 
	}
return energy; 
}

double TotalEnergyPerSpin(int l, int F[][l]) {
	int n = l*l;
	int i,j;	
	int total_energy = 0; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			total_energy += 0.5*EnergyAtSite(l,i,j,F); 			
		}
	}	
return (double)total_energy/n;
}

double EnergyFlipAtSite(int l, int i, int j, int F[][l], int new_spin) {
	int k; 
	int nns = 4; 
	int site = new_spin; 
	int nn_arr[] = {F[i?(i-1):(l-1)][j], F[(i+1)%l][j], F[i][j?(j-1):(l-1)], F[i][(j+1)%l]}; 

	int energy = 0; 
	for(k=0;k<nns;k++) {
		energy -= site*nn_arr[k]; 
	}
return energy; 

}

void MetropolisStep(int l, int F[][l], double T, gsl_rng*r) {
	int i = (int)gsl_rng_uniform_int(r,l); 
	int j = (int)gsl_rng_uniform_int(r,l); 

	int new_spin = 2*(int)gsl_rng_uniform_int(r,2)-1; 
	int delta_energy = EnergyFlipAtSite(l,i,j,F,new_spin) - EnergyAtSite(l,i,j,F); 
	double boltzmann = exp(-delta_energy*(1.0)/T); 
	double random = gsl_rng_uniform(r); 

	if(delta_energy < 0) {
		F[i][j] = new_spin; 
	} else if (delta_energy >= 0) {
		if(random <= boltzmann) {
			F[i][j] = new_spin; 
		} else if (random > boltzmann) {
			F[i][j] *= 1; 
		}
	}
}
