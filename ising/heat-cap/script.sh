#! /bin/bash

./heatcap 4 1000 20000 > heatcap_L4.dat
./heatcap 8 2000 30000 > heatcap_L8.dat
./heatcap 16 3000 40000 > heatcap_L16.dat
./heatcap 24 4000 60000 > heatcap_L24.dat
./heatcap 32 6000 80000 > heatcap_L32.dat
./heatcap 64 7000 100000 > heatcap_L64.dat

gnuplot heatcap.p
