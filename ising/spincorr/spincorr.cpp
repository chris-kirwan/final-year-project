#include <iostream>
#include <vector>
#include <cmath>
#include <numeric>
#include "lattice.h"

void TestLattice(std::vector<std::vector<int>> &a) {
	int i,j; 
	int l = a.size(); 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			a[i][j] = i+j; 
		}
	}
}

void PrintLattice(std::vector<std::vector<int>> &a) {
	int i,j; 
	int l = a.size(); 

	std::cout << std::endl; 
	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			std::cout << a[i][j] << " "; 
		}
		std::cout << std::endl;
	}
	std::cout << std::endl; 
}

int DistanceSquared(int i, int j, int m, int n) {
	return (i-m)*(i-m) + (j-n)*(j-n); 
}	


int DistanceAddToVector(std::vector<std::vector<int>> &a, int r) {
	int m,n; 
	int l = a.size(); 

	int count = 0; 
	for(m=0;m<l;m++) {
		for(n=0;n<l;n++) {
			int dist = DistanceSquared(0,0,m,n); 
			if(dist == r) {
				count++; 
			}
		}
	}
return count; 
}

int TwoPointFunction(std::vector<std::vector<int>> &a, int i, int j, int r) {
	int m,n;
   	int l = a.size(); 	
	int point_1 = a[i][j]; 

	std::vector<int> vector; 
 
	for(m=-2*l;m<2*l;m++) {
		for(n=-2*l;n<2*l;n++) {
			int dist = DistanceSquared(i,j,m,n); 
			if(dist == r) {
				int point_2 = a[(m<0)?(l+m):(m%l)][(n<0)?(l+n):(n%l)];
				vector.push_back(point_1*point_2);
			}
		}
	}
	int sum = std::accumulate(vector.begin(), vector.end(), 0); 
return sum; 
}

int Correlation(std::vector<std::vector<int>> &a, int r) {
	int i,j; 
	int l = a.size(); 
	int sum = 0; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			sum += TwoPointFunction(a,i,j,r); 
		}
	}	
return sum; 
}

int main(int argc, char *argv[]) {
	int i,j; 
	int l = 20; 
	int n = l*l; 
	
	std::vector<std::vector<int>> lattice(l, std::vector<int> (l));
	std::vector<int> radius; 
	
	ColdLattice(lattice); 
	PrintLattice(lattice);
	
	int dist_max = (l/2)*(l/2);

	for(i=0;i<dist_max;i++) {
		std::cout << i << " " <<  DistanceAddToVector(lattice,i) << std::endl;  
	}

	for(i=0;i<dist_max; i++) {
		int ans = DistanceAddToVector(lattice,i); 
		if(ans != 0) {
			radius.push_back(i); 
		}
	}

	for(auto k : radius) {
		std::cout << k << " " << Correlation(lattice, k) << std::endl; 
	}
}
