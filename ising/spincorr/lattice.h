#ifndef LATTICE_H

#define LATTICE_H

void ColdLattice(std::vector<std::vector<int>> &a); 
void RandomLattice(std::vector<std::vector<int>> &a); 
double MagnetizationPerSpin(std::vector<std::vector<int>> &a); 
double EnergyPerSpin(std::vector<std::vector<int>> &a);
int EnergyAtSite(std::vector<std::vector<int>> &a, int i, int j); 
int EnergyFlipAtSite(std::vector<std::vector<int>> &a, int i, int j, int new_spin); 
void MetropolisStep(std::vector<std::vector<int>> &a, double T); 

#endif
