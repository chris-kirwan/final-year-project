#include <stdio.h>
#include <stdlib.h>

void TestLattice(int l, int F[][l]) {
	int i,j; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			F[i][j] = 1; 
		}
	}
}

void PrintLattice(int l, int F[][l]) {
	int i,j; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			printf("%d ", F[i][j]); 
		}
		printf("\n"); 
	}
}

int DistanceSquared(int i, int j, int m, int n) {
	int dist_sqd = (m-i)*(m-i) + (n-j)*(n-j); 
return dist_sqd;
}	


void TwoPointCorr(int l, int F[][l], int i, int j, int r) {
	int m,n; 
	int point_1 = F[i][j]; 
	int counter = 0; 
	double sum = 0.0; 

	for(m=-2*l;m<2*l;m++) {
		for(n=-2*l;m<2*l;m++) {
			int dist = DistanceSquared(int i, int t, int m, int n); 

			if(dist == r) {
				counter++; 
			}
		}
	}

}
int main(int argc, char *argv[]) {
	int l = 10; 
	int n = l*l; 
	int dist_max = (l/2)*(l/2); 

	int (*F)[l] = malloc(sizeof(int[l][l]));
	TestLattice(l,F); 
	PrintLattice(l,F); 

}
