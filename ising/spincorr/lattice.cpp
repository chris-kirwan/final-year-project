#include <iostream>
#include <cmath>
#include <vector>
#include <random>

void ColdLattice(std::vector<std::vector<int>> &a) {
	int i,j; 
	int l = a.size(); 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			a[i][j] = 1; 
		}
	}
}

void RandomLattice(std::vector<std::vector<int>> &a) {
	int i,j; 
	int l = a.size(); 

	std::random_device rd; 
	std::mt19937 mt(rd()); 
	std::uniform_int_distribution<> rnd_sigma(0,1); 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			a[i][j] = 2*(rnd_sigma(mt)) - 1; 
		}
	}
}

double MagnetizationPerSpin(std::vector<std::vector<int>> &a) {
	int i,j; 
	int l = a.size(); 
	int n = l*l; 

	double magnetization = 0.0; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			magnetization += a[i][j]; 
		}
	}
return magnetization/n; 
}

int EnergyAtSite(std::vector<std::vector<int>> &a, int i, int j) {
	int l = a.size(); 

	int at = a[i][j]; 
	int n_1 = a[i?(i-1):(l-1)][j]; 
	int n_2 = a[(i+1)%l][j]; 
	int n_3 = a[i][j?(j-1):(l-1)]; 
	int n_4 = a[i][(j+1)%l]; 

	int energy = -at*(n_1 + n_2 + n_3 + n_4); 
return energy; 
}

int EnergyFlipAtSite(std::vector<std::vector<int>> &a, int i, int j, int new_spin) {
	int l = a.size(); 

	int at = new_spin; 
	int n_1 = a[i?(i-1):(l-1)][j]; 
	int n_2 = a[(i+1)%l][j]; 
	int n_3 = a[i][j?(j-1):(l-1)]; 
	int n_4 = a[i][(j+1)%l]; 

	int energy = -at*(n_1 + n_2 + n_3 + n_4); 
return energy; 
}

double EnergyPerSpin(std::vector<std::vector<int>> &a) {
	int i,j; 
	int l = a.size(); 
	int n = l*l; 

	double total_energy = 0.0; 
	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			total_energy += 0.5*EnergyAtSite(a,i,j); 
		}
	}
return total_energy/n; 
}


void MetropolisStep(std::vector<std::vector<int>> &a, double T) {
	int l = a.size();

	std::random_device rd; 
	std::mt19937 mt(rd());
	std::uniform_int_distribution<> rnd_sigma(0,1);
	std::uniform_int_distribution<> rnd_point(0,l-1);	
	std::uniform_real_distribution<> rnd_realn(0.0,1.0);

	int i = rnd_point(mt); 
	int j = rnd_point(mt); 

	int new_spin = 2*rnd_sigma(mt) -1; 
	int delta_energy = EnergyFlipAtSite(a,i,j,new_spin) - EnergyAtSite(a,i,j);
	double boltzmann = exp(-delta_energy * ((1.0)/T));
   	double rnd_num = rnd_realn(mt); 	

	if(delta_energy < 0) { 
		a[i][j] = new_spin; 
	} else if (delta_energy >= 0) {
		if(rnd_num <= boltzmann) {
			a[i][j] = new_spin; 
		} else if (rnd_num > boltzmann) {
			a[i][j] *=1; 
		}
	}
}
