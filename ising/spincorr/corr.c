#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include "../ising.h"

void TestLattice(int l, int F[][l]) {
	int i,j; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			F[i][j] = l*i+j; 
		}
	}
}

void PrintLattice(int l, int F[][l]) {
	int i,j; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			printf("%d ", F[i][j]); 
		}
		printf("\n");
	}
}

int DistanceSquared(int i, int j, int m, int n) {
	int dist = (m-i)*(m-i) + (n-j)*(n-j);
return dist; 
}

double TwoPointCorr(int l, int F[][l], int i, int j, int r) {
	int m,n; 
	int point_1 = F[i][j]; 
	double sum = 0.0; 
	int counter = 0; 

	for(m=-2*l;m<2*l;m++) {
		for(n=-2*l;n<2*l;n++) {
			int dist = DistanceSquared(i,j,m,n); 
			
			if(dist == r) {
				int point_2 = F[(m<0)?(l+m):(m%l)][(n<0)?(l+n):(n%l)];
				sum += point_1*point_2; 
				counter++; 
			}
		}
	} 

if(counter != 0) {
	return sum/counter; 
} else if (counter == 0) {
	return 0; 
}
}

double FullCorr(int l, int F[][l], int r) {
	int i,j; 
	int n = l*l; 
	double sum = 0.0;

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			sum += TwoPointCorr(l,F,i,j,r); 
		}
	}
return sum/n; 
}

int main(int argc, char* argv[]) {
	int l = 20; 
	int n = l*l; 
	int dist_max = (l/2)*(l/2); 
	int i,j,x,y; 

	//int therm = 5000; 
	int mc_steps = 10; 
	//double T = atof(argv[1]); 

	int (*F)[l] = malloc(sizeof(int[l][l]));

	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);
	unsigned long int seed = (unsigned)time(NULL);
	gsl_rng_set(r, seed);

	RandomLattice(l,F,r);

	//for(i=0;i<therm;i++) {
	//	for(j=0;j<n;j++) {
	//		MetropolisStep(l,F,T,r);
	//	}
	//}

	double *corr_arr = (double*)malloc(dist_max * sizeof(double)); 
	double tol = 0.0E-14; 
	for(i=0;i<mc_steps;i++) {
		//for(j=0;j<n;j++) {
		//	MetropolisStep(l,F,T,r);
		//}
		int dist_sqd; 
		for(dist_sqd=0;dist_sqd<dist_max;dist_sqd++) {	
			corr_arr[dist_sqd] += FullCorr(l,F,dist_sqd);	
		}
	}

	for(i=0;i<dist_max;i++) {
		double ans = corr_arr[i]; 
		if(ans > tol) {printf("%d %lf\n", i, ans);} 
	}

free(corr_arr); 
return 0; 
}


