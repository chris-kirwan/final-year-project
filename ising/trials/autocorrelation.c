#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include "../ising.h"

double Mean(double array[], int len) {
	int i; 
	double sum = 0.0; 

	for(i=0;i<len;i++) {
		sum += array[i]; 
	}
return sum/len; 
}

double AutoCorr(double array[], double len, int t) {
	int i; 
	int max_len = len-t; 

	double mu = Mean(array, len); 
	double sum = 0.0; 
	int counter = 0; 

	for(i=0;i<max_len;i++) {
		double term_1 = array[i] - mu; 
		double term_2 = array[i+t] - mu; 
		sum += term_1*term_2; 
		counter++; 
	}
return (1.0/counter)*(sum); 
}

void Metropolis(int l, int F[][l], int t1, int t2, int t3,
				double a[], double b[], double T, gsl_rng*r) {
	
	int i,j,t;
	int n = l*l; 

	for(i=0;i<t1;i++) {
		for(j=0;j<n;j++) {
			MetropolisStep(l,F,T,r); 
		}
	}

	for(i=0;i<t2;i++) {
		for(j=0;j<n;j++) {
			MetropolisStep(l,F,T,r); 
		}
		a[i] = fabs(MagnetizationPerSpin(l,F)); 
	}

	for(t=0;t<t3;t++) {
		double ans = AutoCorr(a,t2,t); 
		double norm = AutoCorr(a,t2,0); 
		b[t] = ans/norm; 
	}
}

int main(int argc, char* argv[]) {
	int l = atoi(argv[1]); 
	int n = l*l; 

	double T = atof(argv[2]); 

	int thermalization = 5000; 
	int mc_steps = atoi(argv[3]);
   	int t_max  = atoi(argv[4]); 	

	int (*F)[l] = malloc(sizeof(int[l][l])); 

	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937); 
	unsigned long int seed = (unsigned)time(NULL); 
	gsl_rng_set(r, seed); 

	RandomLattice(l,F,r);

	double *mag_arr = (double*) malloc(mc_steps * sizeof(double)); 
	double *auto_corr = (double*) malloc(t_max * sizeof(double));

	Metropolis(l,F,thermalization,mc_steps,t_max,mag_arr,auto_corr,T,r); 

	for(int t=0;t<t_max;t++) {
		printf("%d %lf\n", t, auto_corr[t]); 
	}

free(mag_arr); 
free(auto_corr); 
free(F); 
return 0; 
}


