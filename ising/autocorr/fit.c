#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double ExponentialFit(double a, double b, int x) {
	return exp(a+b*x); 
}

int main(int argc, char *argv[]) {
	int i; 
	double a = atof(argv[1]); 
	double b = atof(argv[2]); 
	int l = 1000;  
	
	for(i=0;i<l;i++) {
		double ans = ExponentialFit(a,b,i); 
		printf("%d %lf\n",i,ans);
	}
}
