#! /bin/bash

./autocorrelation 2.3 5000000 1000 > autocorrelation_23.dat
./autocorrelation 2.4 5000000 1000 > autocorrelation_24.dat
./autocorrelation 2.5 5000000 1000 > autocorrelation_25.dat
./autocorrelation 2.6 5000000 1000 > autocorrelation_26.dat
./autocorrelation 2.7 5000000 1000 > autocorrelation_27.dat
./autocorrelation 2.8 5000000 1000 > autocorrelation_28.dat
./autocorrelation 3.0 5000000 1000 > autocorrelation_30.dat
./autocorrelation 3.5 5000000 1000 > autocorrelation_35.dat

gnuplot autocorrelation.p
