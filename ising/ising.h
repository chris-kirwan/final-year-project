#ifndef ISING_H

#define ISING_H
void ColdLattice(int l, int F[][l]); 
void RandomLattice(int l, int F[][l], gsl_rng *r); 
double MagnetizationPerSpin(int l, int F[][l]); 
int EnergyAtSite(int l, int i, int j, int F[][l]); 
double TotalEnergyPerSpin(int l, int F[][l]); 
int EnergyFlipAtSite(int l, int i, int j, int F[][l]); 
void MetropolisStep(int l,int F[][l], double T, gsl_rng *r); 

#endif
