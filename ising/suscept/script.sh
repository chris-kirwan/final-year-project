#! /bin/bash

./susceptibility 4 1000 40000 > sus_L4.dat
./susceptibility 8 1500 80000 > sus_L8.dat
./susceptibility 16 2000 100000 > sus_L16.dat
./susceptibility 24 2500 200000 > sus_L24.dat
./susceptibility 32 3500 500000 > sus_L32.dat
./susceptibility 64 5000 750000 > sus_L64.dat

gnuplot susceptibility.p
