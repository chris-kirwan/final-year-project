#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include "../ising.h"

double ArrayAverage(int len, double array[]) {
	double sum = 0.0; 
	for(int i=0;i<len;i++) {
		sum += array[i]; 
	}
return sum/len; 
}

void Jackknife(int len, double a_1[], double a_2[], int jack) {
	int i; 

	for(i=0;i<jack-1;i++) {
		a_2[i] = a_1[i]; 
	}
	for(i=jack-1;i<len-1;i++) {
		a_2[i] = a_1[i+1]; 
	}
}

double JackErr(int len, double a[]) {
	double mean = ArrayAverage(len,a); 
	double sumsq = 0.0; 

	double *jack_arr = malloc((len-1) * sizeof(double));
	for(int i=0;i<len;i++) {
		Jackknife(len,a,jack_arr,i+1);
		double jack_mean = ArrayAverage(len-1,jack_arr);
		sumsq += (jack_mean - mean) * (jack_mean -mean);
	}
	sumsq *= (double)(len-1)/((double)len);
free(jack_arr); 
return sqrt(sumsq); 
}

void BinArray(int len, double a[], double b[], int bin_size) {
	int i,j,k=0; 

	for(i=0;i<len;i+=bin_size) {
		double sum = 0.0; 
		int count = 0;  

		for(j=0;j<bin_size;j++) {
			sum += a[i+count]; 
			count++; 
		}
		b[k] = sum/bin_size; 
		k++; 
	}
}

int main(int argc, char* argv[]) {
	int l = atoi(argv[1]); 
	int n = l*l; 
	int i,j; 

	int (*F)[l] = malloc(sizeof(int[l][l])); 
	
	int therm = atoi(argv[2]); 
	int mc_steps = atoi(argv[3]);
	int bin_size = 1000; 

	 gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);
	 unsigned long int seed = (unsigned)time(NULL);
	 gsl_rng_set(r, seed);
	
	double T; 

	for(T=2.0;T<2.8;T+=0.01) {
		double *mag_arr = (double*) malloc(mc_steps * sizeof(double));
		double *mag_sqd = (double*) malloc(mc_steps * sizeof(double));
		double *bin_arr_1 = (double*) malloc(mc_steps/bin_size * sizeof(double)); 
		double *bin_arr_2 = (double*) malloc(mc_steps/bin_size * sizeof(double)); 

		ColdLattice(l,F); 

		for(i=0;i<therm;i++) {
			for(j=0;j<n;j++) {
				MetropolisStep(l,F,T,r); 
			}
		}

		for(i=0;i<mc_steps;i++) {
			for(j=0;j<n;j++) {
				MetropolisStep(l,F,T,r); 
			}
			double ans = MagnetizationPerSpin(l,F); 
			mag_arr[i] = fabs(ans);
		   	mag_sqd[i] = ans*ans; 	
		}
		
		BinArray(mc_steps,mag_arr,bin_arr_1,bin_size);
	   	BinArray(mc_steps,mag_sqd,bin_arr_2,bin_size); 	
		double err_1 = JackErr(mc_steps/bin_size, bin_arr_1); 
		double err_2 = JackErr(mc_steps/bin_size, bin_arr_2); 
		double mag = ArrayAverage(mc_steps,mag_arr); 
		double mag2 = ArrayAverage(mc_steps,mag_sqd); 
		
		double ans = (n/T) * (mag2 - mag*mag); 
		double err = (n/T) * sqrt(4*mag*mag*err_1*err_1 + err_2*err_2); 
		printf("%lf %lf %lf\n", T,ans,err); 
		
		free(mag_arr);
		free(bin_arr_1); 
	}

free(F); 
return 0; 
}
