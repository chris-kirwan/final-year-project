import matplotlib.pyplot as plt 
import numpy as np


a = np.loadtxt("initiallattice.dat")
b = np.loadtxt("newlattice.dat")


fig = plt.figure(frameon=False)
ax1 = fig.add_subplot(121)
plt.imshow(a, cmap='hsv', interpolation='none')
plt.axis('off')

ax2 = fig.add_subplot(122)
plt.imshow(b, cmap='hsv',interpolation='none')
plt.axis('off')

fig.tight_layout()
plt.show()
