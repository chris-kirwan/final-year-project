import numpy as np
import matplotlib.pyplot as plt

X, Y = np.meshgrid(np.arange(-1, 1, .1), np.arange(-1, 1, .1))
U = -Y/(X**2 + Y**2)
V = X/(X**2 + Y**2)

#Normalize the arrows:
U = U / np.sqrt(U**2 + V**2);
V = V / np.sqrt(U**2 + V**2);


plt.figure()
plt.title('Normalized arrows')
Q = plt.quiver(X, Y, U, V, units='width')
qk = plt.quiverkey(Q, 0.9, 0.9, 2, r'$2 \frac{m}{s}$', labelpos='E',
                   coordinates='figure')

plt.show()
