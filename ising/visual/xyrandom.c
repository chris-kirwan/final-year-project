#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include "../xy.h"

void PrintLattice(int l, double F[][l]) {
	int i,j; 

	for(i=0;i<l;i++) {
		for(j=0;j<l;j++) {
			printf("%lf ", F[i][j]); 
		}
		printf("\n"); 
	}
}

int main(int argc, char* argv[]) {
	int l = 256; 
	int n = l*l; 
	int i,j; 

	double T = 0.04; 

	double (*F)[l] = malloc(sizeof(double[l][l])); 

	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937); 
	unsigned long int seed = (unsigned)time(NULL); 
	gsl_rng_set(r, seed); 

	RandomLattice(l,r,F); 
	
	PrintLattice(l,F); 

free(F); 
return 0; 
}


