#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include "../spinlattice.h"

double Average(int len, double array[]) {
	int i;
	double sum = 0.0; 

	for(i=0;i<len;i++) {
		sum += array[i]; 
	}
return sum/len; 
}


//double TotalSum(int len, double array[]) {
//	double sum = 0.0; 
//	int i; 
//	for(i=0;i<len;i++) {
//		sum += array[i];
//	}
//return sum; 
//}

//double Jackknife(int len, double array[], int i) {
//	double sum = TotalSum(len,array); 
//	double xbar_i = sum - array[i]; 
//return xbar_i/(len-1); 
//}

//double JackVar(int len, double array[]) {
//	double sum = 0.0; 
//	double term1; 
//	int i; 
//
//	for(i=0;i<len;i++) {
//		term1 = Jackknife(len,array,i) - TotalSum(len,array)/len; 
//		sum += term1*term1; 
//	}
//return ((len-1)/(double)(len)) * sum; 
//}


int main(int argc, char* argv[]) {
	int l = atof(argv[1]); 
	int n = l*l; 
	int i,j; 

	int thermalization = 5000;
	int monte_carlo_steps = 500000; 
	int sampling_frequency = 100; 
	int number_samples = monte_carlo_steps/sampling_frequency; 

	int (*F)[l] = malloc(sizeof(int[l][l])); 

	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937); 
	unsigned long int seed = (unsigned)time(NULL); 
	gsl_rng_set(r, seed); 

	double T; 
	for(T=2.0;T<2.6;T+=0.0125) {
		ColdLattice(l,F);

	   	double mag_sqrd_array[number_samples];
	   	double mag_4th_array[number_samples]; 	
		int count = 0; 

		for(i=0;i<thermalization;i++) {
			for(j=0;j<n;j++) {
				MetropolisStep(l,F,T,r); 
			}
		}

		for(i=0;i<monte_carlo_steps;i++) {
			for(j=0;j<n;j++) {
				MetropolisStep(l,F,T,r); 
			}
			if(!(i%sampling_frequency)) { 
				mag_sqrd_array[count] = MagnetizationPerSpin(l,F)*MagnetizationPerSpin(l,F); 
				mag_4th_array[count] = MagnetizationPerSpin(l,F)*MagnetizationPerSpin(l,F)
										*MagnetizationPerSpin(l,F)*MagnetizationPerSpin(l,F); 
				count++; 
			}
		}
		double avg1 = Average(count, mag_sqrd_array); 
		double avg2 = Average(count, mag_4th_array); 

		//double var1 = JackVar(count, mag_array); 
		//double var2 = JackVar(count, mag_sqrd_array); 

		printf("%lf %lf\n", T, 1 - avg2/(3.0*avg1*avg1)); 
	}

free(F); 
return 0; 
}


