\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Phase Transitions \& Spin Models}{4}{chapter.1}%
\contentsline {section}{\numberline {1.1}Introduction}{4}{section.1.1}%
\contentsline {section}{\numberline {1.2}Definitions and Conventions}{5}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Hamiltonians}{6}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Boundary Conditions}{6}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Correlation Functions}{7}{subsection.1.2.3}%
\contentsline {chapter}{\numberline {2}Results from Statistical Mechanics}{8}{chapter.2}%
\contentsline {section}{\numberline {2.1}The Ising Model}{8}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Observables}{8}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Phase Transitions in the Ising Model}{9}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}The XY Model}{9}{section.2.2}%
\contentsline {chapter}{\numberline {3}Monte Carlo Simulations}{11}{chapter.3}%
\contentsline {section}{\numberline {3.1}The Monte Carlo Method}{11}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}The Estimator}{11}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Importance Sampling}{12}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Markov Process}{13}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Detailed Balance \& Ergodicity}{13}{subsection.3.1.4}%
\contentsline {section}{\numberline {3.2}Algorithms}{15}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Metropolis Algorithm}{15}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Wolff Cluster Algorithm}{17}{subsection.3.2.2}%
\contentsline {subsubsection}{Critical Slowing Down}{17}{section*.6}%
\contentsline {subsubsection}{The Algorithm}{17}{section*.8}%
\contentsline {chapter}{\numberline {4}Computational Results for the Ising Model}{20}{chapter.4}%
\contentsline {section}{\numberline {4.1}Autocorrelations}{20}{section.4.1}%
\contentsline {section}{\numberline {4.2}Thermalization}{21}{section.4.2}%
\contentsline {section}{\numberline {4.3}Statistical Errors}{23}{section.4.3}%
\contentsline {section}{\numberline {4.4}Observables}{24}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Magnetization}{24}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}Energy}{25}{subsection.4.4.2}%
\contentsline {subsection}{\numberline {4.4.3}Susceptibility and Heat-Capacity}{25}{subsection.4.4.3}%
\contentsline {subsection}{\numberline {4.4.4}The Binder Cumulants}{25}{subsection.4.4.4}%
\contentsline {subsection}{\numberline {4.4.5}Critical Slowing Down}{26}{subsection.4.4.5}%
\contentsline {chapter}{\numberline {5}Computational Results for the XY Model}{29}{chapter.5}%
\contentsline {section}{\numberline {5.1}Metropolis Algorithm}{29}{section.5.1}%
