\chapter{Computational Results for the Ising Model}
\section{Autocorrelations}
	In performing measurements for the Metropolis and Wolff Cluster algorithms, we have assumed that 
	the indiviudal states along the Markov chain are independent of each other. However, this assumption
	is false, as flipping a spin from one state to the next depends on the first state. Therefore, even 
	though the Markov process is a memoryless process, entries in the chain are statistically dependent.
	Carrying out statistical analysis on the Markov chain must therefore be done with caution. 
	The main question now becomes how many timesteps/sweeps does it take to estimate $\langle Q \rangle$
	to a sufficient accuracy?

	Using the estimator of $\langle Q \rangle$, Equation \ref{eqn:estimator}, 
	\begin{equation*}
		Q_m = \frac{1}{m} \sum_{i=1}^{m} Q_i
	\end{equation*}
	with the time series of measurements $Q_i = Q_{\mu_i} \ i=1,2,\dots,m$ from the Markov 
	process. The time elapsed between each measurements $Q_i and Q_{i+1}$ must remain constant and 
	be independent of $i$. 

	The \textit{autocorrelation function} measures how rapidly a quantity $Q_i$ becomes statistically 
	independent from another previous measurement along the Markov chain. The autocorrelation function is
	defined as 
	\begin{equation}
	A_Q(t) = \frac{\langle Q_{i+t} Q_{t} \rangle- \langle Q_i \rangle^2}{\langle Q_i^2 \rangle 
	- \langle Q_i \rangle^2},
	\end{equation}
	where $\langle \cdot \rangle$ refers to the average value over the configurations in the sample,
	i.e. dividing by $m-t$. Note that $A_q(0) = 1$, which is perfectly sensible as a configuration is 
	completely correlated with itself \cite{binder_monte_1986}.

	The asymptotic behaviour of $A_Q(t)$ for large $t$ is 
	\begin{equation}
		A_Q(t) \sim e^{(\frac{-t}{\tau})} \  \mathrm{for} \ t \rightarrow \infty
	\end{equation}
	where $\tau$ is called the \textit{(exponential) correlation time} and is related to the second
	largest eignevalue $\lambda_1$\footnotemark of the Markov matrix $P$ by 
	\[ \tau = \frac{-1}{\ln(\lambda_1)} \]
	Conventionally, measurements are taken $2\tau$ apart, rather than one $\tau$, as the autocorrelation
	is a factor $\frac{1}{e} \approx 0.37$ smaller than the maximum correlation. For $2\tau$ this falls
	to $\frac{1}{e^2} \approx 0.14$. 

	\footnotetext{Assumpting that $Q$ has a non-zero projection on the corresponding eigenstate.}

	\begin{figure}
	\centering
	\begin{subfigure}{.5\textwidth}
  		\centering
  		\includegraphics[width=\linewidth]{chapters/images/ising/autocorrelation.png}
  		\caption{Log Autocorrelation for the Metropolis Algorithm}
  		\label{fig:autocorr1}
	\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
  		\centering
		\includegraphics[width=\linewidth]{chapters/images/wolff/autocorrelation.png}
  		\caption{Log Autocorrelation for the Wolff Cluster Algorithm}
  		\label{fig:autocorr2}
	\end{subfigure}
	\caption{Log of the magnetization autocorrelation function: $\ln(A_M(t))$ for various values
	of temperature above $T_c$. The black lines are exponential fits of the data. The inset plot is
	$A(t)$.}
	\label{fig:test}
	\end{figure}

	Alternatively, one can calculate the \textit{integrated autocorrelation time} $\tau_{\mathrm{int}}$. 
	This is defined as \cite{binder_monte_1986}
	\begin{equation}
		\tau_{\mathrm{int}} = \left( 1+ 2\sum_{t=1}^{m-1} \left( 1 - \frac{t}{m} \right) A_{Q}(t) \right).
	\end{equation}
	In the majority of simulations, one is interested in the limit $m \rightarrow \infty$ and 
	$\tau_{\mathrm{int}}$ becomes
	\begin{equation}
		\tau_{\mathrm{int}} = \left( \frac{1}{2} + \sum_{t=1}^{\infty} A_Q(t) \right).
	\end{equation}

	
	
\section{Thermalization}
	As stated in Section 3.2.1, a system in thermal equilibrium has small fluctuations in the total 
	energy around its' thermal average, therfore it's region of state space is restricted. In choosing
	an intial state to start the system in, espcially when those intital states are far from the desired
	states, one must perform a "thermalizing" process through state sapce to arrive at the desired
	one. 

	Two problems appear immediately when accounting for thermalization. They are 
	\begin{enumerate}
		\item What starting conditions, ie: hot or cold conditions, are most appropriate when appyling 
			the Metropolis or Wolff-Cluster algorithms, especially when trying to reduce thermalization
			time. 
		\item What are the criteria that determine if the system has been thermalized. 
	\end{enumerate}
	Hot and cold starts correspond to the initial spin configuratios of the Monte Carlo simulations. In 
	a cold start, every spin aligns in the same direction and so the temperature of the lattice is zero. 
	Opposingly, a hot start has every spin pointing in a random orientation. The temperature of this
	lattice is therefore effectively infinite. A comparision for the two starting conditions using 
	the metropolis algorithm is made in \ref{fig:therm1}

	\begin{figure}
	\centering
	\begin{subfigure}{.5\textwidth}
  		\centering
  		\includegraphics[width=\linewidth]{chapters/images/ising/thermalization.png}
  		\caption{A hot start for the $2-d$ Ising Model on the square lattice.}
  		\label{fig:hotstart1}
	\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
  		\centering
		\includegraphics[width=\linewidth]{chapters/images/ising/thermalization_cold.png}
  		\caption{A cold start for the same model.}
  		\label{fig:coldstart2}
	\end{subfigure}
	\caption{A comparision of the hot and cold starts for the Metropolis algorithm for 
			a variery of temperatures. $l=24$}
	\label{fig:therm1}
	\end{figure}

	Seeding the random number generator\footnotemark for the hot start with different numbers can also 
	give us a method to determine whether the system is sufficiently thermalized. Thermalization is 
	considered to have occured when each run has converged on a common value of whatever observable is 
	being monitored, at that particular temperature. In the case of Figures \ref{fig:therm1} and 
	\ref{fig:randomseeds}, it is the magnetization. As a quick aside, Usually, the seed for random 
	number generation is set using Cs' built in \verb|time()| function. But as Figure 
	\ref{fig:randomseeds} was computed so quickly, the seed was actually stuck on the 
	same value. So large numbers were used to seed the generator. 

	\begin{figure}
		\centering
		\includegraphics[width=0.6\textwidth]{chapters/images/ising/therm.png}
		\caption{Thermalization of $4$ hot starts with a different seed, for $T=1.3$ and $l=24$. When
		the values of the observables (this case, the magnetization) converge, we say that the system 
		has thermalized.}
		\label{fig:randomseeds}
	\end{figure}

	\section{Statistical Errors}
	Statistical methods are utilized to obtain metrics that assess the quality of the measured 
	observable. We can also get a reasonably accurate picture of how much computational resources 
	are needed to get measurements to a specified accuracy. The analysis of statistical errors created 
	by the simulations was carried out using the \textit{Jackknife Resampling} method. 

	The jackknife resampling is a simplistic, but reasonalby powerful method, of variation and bias
	estimation of correlated data. The estimate of an observable $Q_m$ can be found by calculating how 
	much $Q_m$ varies by removing one element from a set of measurements, such as the Markov chain. 

	So, if the paramater being estimated is the mean of the measurements, and calling this parameter
	$x$, we compute the mean $\bar{x}_i$ of each subsample consisting of all but the 
	$i$-th data point \cite{newman_monte_1999}.
	\begin{equation}
		\bar{x}_i =\frac{1}{n-1} \sum_{j=1,j \neq i}^n x_j, \quad \quad i=1, \dots ,n.
	\end{equation}
	In particular, the mean of the measurements, $\bar{x}$, is given by the average of all $\bar{x}_i$
	\begin{equation}
		\bar{x} = \frac{1}{n} \sum_{i=1}^{n} \bar{x}_i
	\end{equation}
	A jackknife estimate of the variance of the estimator can be calculated from the variance of the 
	collection of $\bar{x}_i$s. 
	\begin{equation}
		\operatorname{Var}(\bar{x})=(n-1) \sum_{i=1}^n (\bar{x}_i - \bar{x})^2 
			=\frac{1}{(n-1)} \sum_{i=1}^n (x_i - \bar{x})^2.
	\end{equation}

	\section{Observables}
	The Ising Model was simulated using both the Metropolis and Wolff Cluster algorithms. Lattices of
	size $L = \{4,8,16,24,32,64,128\}$ were used, with the exception of $l=128$ for the Metropolis
	algorithm simply due to the large number of computations needed. Each simulation was ran for 
	a range of temperatures, typically $T \in [0,5]$, and the cold start was used throughout. This
	was due to the rapid nature of the lattice becoming thermalized, particularly when away from 
	$T_c$. The length of the thermalization period varied as the lattice sizxe grew, starting at
	approximately $1000$ time steps for the $4\times4$ and $8\times8$, up to $20000$ time steps for
	the largest lattices. 

	\subsection{Magnetization}
	\begin{figure}[ht]
	\centering
	\begin{subfigure}{.5\textwidth}
  		\centering
  		\includegraphics[width=\linewidth]{chapters/images/ising/magnetization.png}
		\caption{The Metropolis Algorithm}
  		\label{fig:mag1}
	\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
  		\centering
		\includegraphics[width=\linewidth]{chapters/images/wolff/magnetization.png}
  		\caption{The Wollf-Cluster Algorithm}
  		\label{fig:mag2}
	\end{subfigure}
	\caption{Spontaneous Magnetization of the Ising Model for both Metropolis and Wolff-Cluster 
	Algorithms, for various lattice sizes $L$. The vertical black line indicates $T_c$}
	\label{fig:magnet}
	\end{figure}

	In taking measurements of the spontaneous magnetization, we redefine exactly what we are measuring. 
	Instead of taking the estimator of the magnetization  as $\langle M \rangle = \frac{1}{L^2} M$, with
	$M = \sum_i \sigma_i$, we take 
	\[ M = \left| \sum_i \sigma_i \right| \]
	as the Hamiltonian (Equation \ref{eqn:isingboi}) is symmetric under exchange of $\sigma \rightarrow
	- \sigma $. These configurations will have opposite magnetizations, therefore $\langle M \rangle = 
	\langle \sum_i \sigma_i \rangle = 0$. However, note that for $T > T_c$, this will mean that the 
	estimated value of $\langle M \rangle$ will be overestimated. But for calculations around the 
	critical temperature, this overestimation is of little relevance. 

	Below the critical temperature, note how the two models coincide reasonably well with analytic 
	solution to the spontaneous magnetization in the thermodynamic limit. However, it is clear 
	that the Wolff-Cluster algorithm desribes the spontaneous magnetization much more accurately. 
	Even though both do not show a pahse transition as they are finite systems, the order paramater
	seems to have a much sharper transition for the Wolff Algorithm as it avoids critical slowing down
	much better than the Metropolis algorithm. 

	The error bars were calculated using the Jackknife method as stated above. An important note is that 
	small autocorrelations, that is temperatures away from $T_c$, implies that samples could be taken 
	from the system at a quicker pace, which lead to smaller error bars. Around $T_c$ the autocorrelation
	grows, so fewer samples could be taken, and therefore larger errorbars. 

	\subsection{Energy}
	\begin{figure}[ht]
	\centering
	\begin{subfigure}{.5\textwidth}
  		\centering
  		\includegraphics[width=\linewidth]{chapters/images/ising/energy.png}
		\caption{The Metropolis Algorithm}
  		\label{fig:ene1}
	\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
  		\centering
		\includegraphics[width=\linewidth]{chapters/images/wolff/energy.png}
  		\caption{The Wollf-Cluster Algorithm}
  		\label{fig:ene2}
	\end{subfigure}
	\caption{The energy per site for various lattice sizes $L$. The black line represents $T_c$. The
	variability of the energy for various lattice sizes is much smaller than for the magnetization}
	\label{fig:energy}
	\end{figure}

	\subsection{Susceptibility and Heat-Capacity}
	
	\begin{figure}[ht]
	\centering
	\begin{subfigure}{.5\textwidth}
  		\centering
  		\includegraphics[width=\linewidth]{chapters/images/ising/susceptibility.png}
		\caption{The Metropolis Algorithm}
  		\label{fig:sus1}
	\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
  		\centering
		\includegraphics[width=\linewidth]{chapters/images/wolff/susceptibility.png}
  		\caption{The Wollf-Cluster Algorithm}
  		\label{fig:sus2}
	\end{subfigure}
	\caption{The energy per site for various lattice sizes $L$. The black line represents $T_c$. The
	variability of the energy for various lattice sizes is much smaller than for the magnetization}
	\label{fig:suscept}
	\end{figure}

	\begin{figure}[ht]
	\centering
	\begin{subfigure}{.5\textwidth}
  		\centering
  		\includegraphics[width=\linewidth]{chapters/images/ising/heatcap.png}
		\caption{The Metropolis Algorithm}
  		\label{fig:heat1}
	\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
  		\centering
		\includegraphics[width=\linewidth]{chapters/images/wolff/heatcap.png}
  		\caption{The Wollf-Cluster Algorithm}
  		\label{fig:heat2}
	\end{subfigure}
	\caption{The energy per site for various lattice sizes $L$. The black line represents $T_c$. The
	variability of the energy for various lattice sizes is much smaller than for the magnetization}
	\label{fig:heatcap}
	\end{figure}


	\subsection{The Binder Cumulants}
	The Binder Cumulants were introduced by Kurt Binder\cite{binder_monte_1986}, to accurately 
	determine the points at which phase transitions occur during numerical simulations of lattice
	systems. The cumulant itself is defied as
	\begin{equation}
		U_4 = 1 - \frac{\langle s^4 \rangle_L }{3\langle s^2 \rangle_L^4},
	\end{equation}
	where $L$ is the size of the lattice and $s$ is order parameter of the system. As discussed above, 
	the order parameter of the Ising Model is simply the spontaneous magentization $M$. The critical 
	temperature is the unique point where the curves for various $L$ cross in the thermodynamic limit.

	\begin{figure}[ht]
	\centering
	\begin{subfigure}{.5\textwidth}
  		\centering
  		\includegraphics[width=\linewidth]{chapters/images/ising/binder.png}
		\caption{The Metropolis Algorithm}
  		\label{fig:bindb1}
	\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
  		\centering
		\includegraphics[width=\linewidth]{chapters/images/wolff/binder.png}
  		\caption{The Wollf-Cluster Algorithm}
  		\label{fig:bindb2}
	\end{subfigure}
	\caption{The energy per site for various lattice sizes $L$. The black line represents $T_c$. The
	variability of the energy for various lattice sizes is much smaller than for the magnetization}
	\label{fig:binderbig}
	\end{figure}

	\begin{figure}[ht]
	\centering
	\begin{subfigure}{.5\textwidth}
  		\centering
  		\includegraphics[width=\linewidth]{chapters/images/ising/binder_zoomed.png}
		\caption{The Metropolis Algorithm}
  		\label{fig:binds1}
	\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
  		\centering
		\includegraphics[width=\linewidth]{chapters/images/wolff/binder_zoomed.png}
  		\caption{The Wollf-Cluster Algorithm}
  		\label{fig:binds2}
	\end{subfigure}
	\caption{The energy per site for various lattice sizes $L$. The black line represents $T_c$. The
	variability of the energy for various lattice sizes is much smaller than for the magnetization}
	\label{fig:bindersmall}
	\end{figure}


	\subsection{Critical Slowing Down}
	Finally, as discussed in Section 3.2.2 on \textit{Critical Slowing Down}, the times $\tau$ and 
	also $\tau_{\mathrm{int}}$ which can then be used to calculate the critical exponent $z$ for both
	the Metropolis algorithm and Wolff Cluster algorithm. This will then give a good estimate of 
	how much each algorithm is affected by critical slowing down. 
	The results of the calculations are shown in Figure \ref{fig:intcorr} and Table YYY

	\begin{figure}
		\centering
		\includegraphics[width=0.6\textwidth]{chapters/images/ising/integrated.png}
		\caption{Integrated correlation time as function of lattice size $L$, for both 
			the Metropolis and Wolff Cluster Algorithms. The black line indicates exponential fits
		for $z$}
		\label{fig:intcorr}
	\end{figure}

	\begin{table}[]
		\caption{$\xi$ and $C$ about $T_c$}
		\begin{tabular}{lll}
		\hline
		$L$ & $\xi$ & $C$ \\ \hline
		4   &       &     \\
		8   &       &     \\
		16  &       &     \\
		32  &       &     \\
		64  &       &     \\ \hline
\end{tabular}
\end{table}

