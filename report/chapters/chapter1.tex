\chapter{Phase Transitions \& Spin Models}
	\section{Introduction}
	A \textit{phase transition} is described by an abrupt/discontinuous change in the macroscopic 
	behaviour of a large collection of microscopic objects. This change is brought about by adjusting 
	some external parameter, such as temperature or magnetic field strength. The striking aspect of 
	phase transitions is that the smooth, continuous description of classical particles\footnote{
	Such as position and momenta} fails to describe the physics of the macroscopic level. Usually, one
	identifies a phase transition with a discontinuity (or, more generally non-analyticity) in some 
	thermodynamic function ,labelled by an \textit{order parameter}, such as the free energy $f$. The 
	methods of Statistical Mechanics enables us to describe these transitions starting from a 
	microscopic view. 

	In the early 20th century, there were still some serious doubts as to whether a single Hamiltonian 
	describing a system, which does not distingush phases \textit{a priori}, could in fact describe 
	a phase transition. (cf: Van der Waals Centenary Conference, 1937). Studies of what would later be 
	termed \textit{spin models} were first introduced by Ernest Ising in 1925 \cite{ising_beitrag_1925}. 
	This associated a variable (which is now usually referred to as the spin) $\sigma = \pm 1$ on the 
	vertices of a $d$ dimensional lattice $\Lambda \subset \Z^d$. This specifc model was later named the 
	\textit{Ising Model}. See Figure \ref{fig:isingmodel} for reference.  

	\begin{figure}[ht]
		\centering
		\scalebox{0.7}{\incfig{ising1_2}}
		\caption{$(a): 1$-dimensional Ising Model. $(b): 2$-dimensional Ising Model. The arrows indicate
		the direction of the spin, i.e, $\sigma = \pm 1$. The dots indicate that the spins extend to
		infinity}
		\label{fig:isingmodel}
	\end{figure}

	Ising's thesis proved that for $d=1$, no phase transition occurs, as the free energy remains 
	analytic for any positive temperature $T$ and constant magnetic field $h$. 
	The covariance of the spins, also called the correlation function, was proved to decay  exponentially
	for all positive $T$. Ising incorrecly assumed that this meant a phase transition did not occur 
	for higher dimensions\footnotemark. However, in $d \geq 2$, a \textit{second order} phase transition 
	occurs for zero magetic field ($h=0$). That is, a \textit{spontaneous symmmetry breaking} occurs 
	at the critical temperature $T_c$, and the model transitions from a high temperature 
	disorderd state to an ordered state that exhibits \textit{spontaneous magnetization}.
	
	\footnotetext{For brevity, the partition function is always analytic for finite systems, as it is 
	a finite sum of analytic functions. It is in the thermodynamic limit (systems of infinite size) that 
	the singularities occur}

	In 1936, using a clever contour agrument, Rudolph Peierls explicitly showed that a phase transition 
	must occur in the $2$-dimensional Ising Model \cite{peierls_isings_1936}. However, it was only by 
	Lars Osengar's analytic expression (published in 1944) of the free energy of the $2$-$d$ Ising Model, 
	in a zero magnetic field with nearest-neighbour coupling that $T_c$ was established 
	\cite{onsager_crystal_1944}. The correlation length (a quantity derived from the correlation 
	function) $\xi$ was then shown to diverge as $T \rightarrow T_c$. 

	As discussed in \cite{baxter_exactly_2007}, although a phase transition occurs in the $d \geq 2$ 
	Ising Model on a $d$-dimensional hypercube lattice, no analytic solution exists of $d \geq 3$ for 
	any thermodynamic function. Therefore, one must use alternate methods to obtain an approximation
	to these functions. Computer simulations offer an easy method to obtain those approximations. 

	The \textit{XY Model} offers an immediate generalization to the Ising Model with many important 
	properties. First and foremost, the discrete spins of the Ising Model are replaced with continuous
	unit vector spins that lie on a circle, i.e. $\sigma_i \rightarrow \vectr{\sigma}_i = (\cos(\theta_i)
	, \sin(\theta_i))$. Thus, the XY Model displays a continuuous $O(2)$ symmetry. For this reason, 
	a continuous phase transition does not exist. However, in analyzing the correlation function (see 
	\cite{mermin_absence_1966, friedli_statistical_2017}), one finds an infinite-order/topological phase 
	transition called the Berezinskii–Kosterlitz–Thouless transition.

	\section{Definitions and Conventions}
	Let's start by making some general definitions of the spin lattice models. From these definitions, 
	the Ising and XY models can then be specified. 

	((((Let $\Lambda \subset \Z^d$ and identify $\Lambda$ with the graph that contains all edges formed
	by nearest-neighbor pairs of vertices of $\Lambda$, which we call the lattice.)))  We denote those 
	nearest neighbour pairings as $\langle i,j \rangle $. For each vertex $i \in \Lambda$, we 
	associate a random $n$-dimensional unit vector $\vectr{\sigma}_i$, which we call the \textit{spin}. 
	The \textit{spin configuration} $\sigma$ is then just an assignment of spins to each lattice site, 
	ie: $\sigma = \left\{ \vectr{\sigma}_i \right\}$. 

	Usually, there is a coupling constant between each neighbour in the lattice $J_{ij}$. However, 
	the simplest strategy is to set $J_{ij}=J $ for all  $\langle i,j \rangle$. During the simulations 
	of the lattice systems, this constant was set to unity. The \textit{inverse temperature} $\beta$ 
	is defined as 
	\[ \beta = \frac{1}{kT}  \]
	with $k$ being the Boltzmann's constant. It is from this that we have a low temperature implying a 
	high $\beta$ and vice versa. Conventionally (and to avoid large numbers in the computer 
	simulations), we set $k=1$. 

	\subsection{Hamiltonians} 
	\begin{defi}
		The Hamiltonian of an $O(n)$ Spin Model on a $d$-dimensional lattice $\Lambda$ is given by 
		\begin{equation}
			\label{eqn:hamilboi}
			H(\sigma) = - \sum_{\langle i,j \rangle } \vectr{\sigma}_i \cdot 
			\vectr{\sigma_j}
		\end{equation}

		Furthermore, adding a non-zero magnetic field $\vectr{h}_i = (h_1, h_2, \dots h_n)_i$ to each
		lattice site gives the Hamiltonian
		\begin{equation} 
			H(\sigma) = - \sum_{\langle i,j \rangle } \vectr{\sigma}_i \cdot 
			\vectr{\sigma_j} - \sum_j \vectr{h}_i \cdot \vectr{\sigma}_j
		\end{equation}

	\end{defi}

	Setting $n = 1,2$ gives us our Ising and XY models respectively. For the Ising Model case, we 
	identify $\{ \pm \vectr{e}_1 \}$ with $\{ \pm 1 \}$. Therefore, we define our Hamiltonians for 
	the two models. The reason the more general model is called an $O(n)$ Spin Model is that for 
	$R \in O(n)$ multiplying $\sigma$ by  $R$ implies that $H(R\sigma) = H(\sigma)$

	\begin{defi}
		The Hamiltonian of the Ising Model, with constant non-zero magnetic field $h$, on a 
		$d$-dimensional lattice $\Lambda$ with $\sigma_i$ identified as aboce, is given by
		\begin{equation} 
		\label{eqn:isingboi}
		H(\sigma) = -\sum_{\langle i,j \rangle} \sigma_i \sigma_j - h\sum_j \sigma_j 
		\end{equation}
	\end{defi}

	\begin{defi}
		The Hamiltonian of the XY Model, with constant non-zero magnetic field $h$, on a 
		$d$-dimensional lattice $\Lambda$ with $\vectr{\sigma}_i = (\cos(\theta_i), \sin(\theta_i))$ , 
		is given by
		\begin{equation} 
			H(\sigma) = -\sum_{\langle i,j \rangle} \cos(\theta_i - \theta_j) - h\sum_j \cos(\theta_j) 
		\end{equation}
	\end{defi}

	\subsection{Boundary Conditions}
	The concept of boundary conditions 	

	\subsection{Correlation Functions}
	An important function in thermodynamic systems such as these spin models is the correlation function. 
	It is a measure of how correlated the spins in a lattice are and therefore the  radial dependence 
	of the correlation. This radial dependence is then associated with the correlation length $\xi$. The 
	correlation function is positive if the values of the spins fluctuate in the same direction, and it 
	is negative if they fluctuate in opposite directions. It takes zero value if the fluctuations are 
	uncorrelated. The \textit{Two-Point} correlation function between two spins $\sigma_i, \sigma_j$ 
	is defined as 

	\begin{equation}
		\label{eqn:2corrfun}
		G^{(2)}(i,j) \equiv \langle (\sigma_i - \langle \sigma_i \rangle)(\sigma_j - 
		\langle\sigma_j\rangle)\rangle 
	\end{equation}

	By the translation invariance, and in our case, isotropy of the lattice, we can consider $G^{(2)}$ as
	a function of $|i-j|$. Therefore, Equation \ref{eqn:2corrfun} can be written as

	\begin{equation}
		G^{(2)}(r) \equiv \langle \sigma_i \sigma_j \rangle - \langle \sigma_i \rangle^2
	\end{equation}
	with $r = |i-j|$
